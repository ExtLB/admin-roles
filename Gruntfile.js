'use strict';
require('ts-node').register();
require('babel-register')({
});

module.exports = grunt => {
  require('time-grunt')(grunt);
  grunt.initConfig({
    exec: {
      processTypescript: {
        command: 'tsc -d -p .'
      }
    }
  });
  grunt.loadNpmTasks('grunt-exec');
  grunt.registerTask('default', ['exec:processTypescript']);
};

