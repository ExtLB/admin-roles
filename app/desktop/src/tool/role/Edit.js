Ext.define('Client.tool.role.Edit', {
  extend: 'Ext.Tool',
  type: 'admin-role-tool.edit',

});
Client.tool.role.Edit.addStatics({
  config: {
    handler: function (owner, tool, evt) {
      let vm = owner.parentMenu.ownerCmp.getViewModel();
      let editor = Ext.create('Client.view.admin.role.Editor', {
        handler: function(user) {
          user.save({
            success: () => {
              Ext.toast(i18next.t('saa:SAVE_ROLE_SUCCESS'));
            },
            failure: () => {
              Ext.toast(i18next.t('saa:SAVE_ROLE_FAILURE'));
            }
          });
        },
        account: vm.get('account')
      });
      editor.show();
    },
    bind: {
      text: '{"mod-admin-roles:EDIT_ROLE":translate}'
    },
    hidden: true,
    separator: true,
    margin: '10 0 0',
    iconCls: 'x-fa fa-edit'
  }
});
