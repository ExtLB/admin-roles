Ext.define('Client.view.admin.roles.View', {
	// extend: 'Ext.grid.Grid',
  extend: 'Ext.Container',
	xtype: 'admin.roles',
	cls: 'admin-roles',
	requires: [
	  'Ext.XTemplate',
    'Ext.layout.Form',
    'Ext.layout.HBox',
    'Ext.layout.VBox',
    'Ext.dataview.List',
    'Ext.grid.Grid',
    'Ext.panel.Resizer',
    'Ext.form.Panel',
    'Ext.field.TextArea',
    'Ext.field.Email',
    'Ext.field.Password',
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.LoadMask',
    'Ext.data.validator.Email',
    'Client.tool.role.*'
  ],
	controller: {type: 'admin.roles'},
	viewModel: {type: 'admin.roles'},
  perspectives: ['admin'],

  layout: 'hbox',
  items: [{
    xtype: 'container',
    layout: 'vbox',
    width: 300,
    items: [{
      xtype: 'list',
      width: 300,
      reference: 'roleList',
      bind: {
        store: '{roleStore}'
      },
      reference: 'roleList',
      listeners: {
        initialize: 'onRoleListInitialize',
        select: 'onSelectRole',
        deselect: 'onDeselectRole'
      },
      itemConfig: {
        xtype: 'simplelistitem',
        tools: {
          menu: {
            handler: 'onRoleMenu',
            zone: 'end',
            bind: {
              tooltip: '{"mod-admin-roles:ROLE_OPTIONS":translate}'
            },
            iconCls: 'x-fa fa-bars'
          }
        },
        tpl: [
          '<div>{name}</div>'
        ],
      },
    }, {
      xtype: 'toolbar',
      docked: 'top',
      items: [{
        xtype: 'button',
        ui: 'round raised',
        handler: 'onRefreshRoles',
        iconCls: 'x-fa fa-sync'
      }, {
        xtype: 'spacer'
      }, {
        xtype: 'button',
        ui: 'round raised',
        handler: 'onAddRole',
        iconCls: 'x-fa fa-plus'
      }]
    }]
  }, {
    xtype: 'container',
    layout: 'vbox',
    flex: 1,
    items: [{
      xtype: 'grid',
      flex: 1,
      reference: 'roleUserGrid',
      bind: {
        store: '{roleMapStore}'
      },
      masked: {
        xtype: 'loadmask'
      },
      listeners: {
        initialize: 'onRoleUserGridInitialize',
        select: 'onRoleUserGridSelect',
        deselect: 'onRoleUserGridDeselect'
      },
      columns: [{
        bind: {
          text: '{"mod-admin-roles:USER_COLUMN.USERNAME":translate}',
        },
        dataIndex: 'user',
        tpl: '{user.username}',
        flex: 1,
        cell: {userCls: 'bold'}
      }, {
        bind: {
          text: '{"mod-admin-roles:USER_COLUMN.EMAIL":translate}',
        },
        dataIndex: 'user',
        tpl: '{user.email}',
        flex: 2,
        cell: {
          tools: {
            sendEmail: {
              handler: 'onSendRoleUserEmail',
              zone: 'start',
              bind: {
                tooltip: '{"mod-admin-roles:SEND_EMAIL":translate}'
              },
              iconCls: 'x-fa fa-envelope'
            },
            removeRoleMap: {
              handler: 'onRemoveRoleUser',
              zone: 'end',
              bind: {
                tooltip: '{"mod-admin-roles:REMOVE_ACCOUNT_USER":translate}'
              },
              iconCls: 'x-fa fa-times'
            }
          }
        }
      }]
    }, {
      xtype: 'panel',
      reference: 'roleUserPanel',
      flex: 1,
      layout: 'fit',
      resizable: {
        edges: 'n'
      },
      bind: {
        title: '{"mod-admin-roles:EDIT":translate} {"mod-admin-roles:ROLE":translate} {roleList.selection.name}',
      },
      listeners: {
        initialize: 'onRoleUserFormInitialize',
      },
      items: [{
        xtype: 'formpanel',
        reference: 'roleUserForm',
        flex: 1,
        padding: 10,
        layout: 'vbox',
        scrollable: true,
        items: [{
          xtype: 'textfield',
          name: 'name',
          required: true,
          bind: {
            label: '{"mod-admin-roles:ROLE_NAME":translate}',
            value: '{roleList.selection.name}'
          }
        }, {
          xtype: 'textareafield',
          name: 'description',
          required: true,
          flex: 1,
          bind: {
            label: '{"mod-admin-roles:ROLE_DESCRIPTION":translate}',
            value: '{roleList.selection.description}'
          }
        }]
      }, {
        xtype: 'toolbar',
        docked: 'bottom',
        items: [{
          xtype: 'button',
          ui: 'rounded raised action',
          handler: 'onRoleUserCancel',
          bind: {
            disabled: '{!roleList.selection.dirty}',
            text: '{"CANCEL":translate}'
          }
        }, {
          xtype: 'spacer'
        }, {
          xtype: 'button',
          ui: 'rounded raised action',
          handler: 'onRoleUserSave',
          bind: {
            disabled: '{!roleList.selection.dirty}',
            text: '{"SAVE":translate}'
          }
        }]
      }]
    }]
  }],

  roleMenu: { // used by Controller
    xtype: 'menu',
    anchor: true,
    padding: 10,
    minWidth: 300,
    viewModel: {},
    items: [{
      xtype: 'component',
      indented: false,
      bind: {
        data: '{roleData}'
      },
      tpl: ['<div style="width: 75px;">' +
        '<div style="width: 200px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{name}</div>' +
        '<div style="color: grey; width: 200px;">' +
        '<div>Created On: {created:date("Y/m/d H:i:s")}</div>' +
        '<div>Modified On: {modified:date("Y/m/d H:i:s")}</div>' +
        '<div>Description: {description}</div>' +
        '</div>' +
        '</div>']
    }]
  }
  // bind: {
  //   store: '{accountStore}',
  // },
  // selectable: { mode: 'single' },
  // listeners: {
		// select: 'onItemSelected'
  // },

});
