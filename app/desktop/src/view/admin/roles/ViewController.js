Ext.define('Client.view.admin.roles.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.admin.roles',

  onSelectRole: function(dataview, selected, eOpts) {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  vm.set('role', selected);
    let mapParams = Ext.decode(vm.get('extraRoleMapParams.filter'), true);
    mapParams.where.roleId = selected.get('id');
    vm.set('extraRoleMapParams', {filter: Ext.encode(mapParams)});
    setTimeout(() => {
      vm.getStore('roleMapStore').load({
        callback: () => {
          me.lookup('roleUserGrid').setMasked(false);
          me.lookup('roleUserPanel').setMasked(false);
        }
      });
    }, 10);
  },
  onDeselectRole: function(dataview, selected, eOpts) {
	  let me = this;
	  let agGrid = me.lookup('roleUserGrid');
    agGrid.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT")
    });
    let panel = me.lookup('roleUserPanel');
    panel.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT_USER")
    });
  },

  onInitializeRemoveRole: function(sender, eOpts) {
    setTimeout(() => {
      let rec = sender.toolOwner.getRecord();
      switch (rec.get('name')) {
        case 'Administrator':
        case 'Developer':
          sender.setHidden(true);
          break;
        default:
          sender.setHidden(false);
      }
    }, 10);
  },

  removeRoleHandler: function(owner, tool, evt) {
    let vm = owner.parentMenu.ownerCmp.getViewModel();
    let rec = vm.get('role');
    // console.log(owner, tool, evt);
    let template = new Ext.XTemplate(i18next.t('saa:CONFIRM_REMOVE_ROLE_MESSAGE'));
	  Ext.Msg.confirm(i18next.t('saa:CONFIRM_REMOVE_ROLE_TITLE'), template.apply({rec: rec}), (btn) => {
	    if (btn === 'yes') {
	      rec.erase({
          callback: (record, operation, success) => {
            if (success !== true) {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ROLE_ERROR'));
            } else {
              Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ROLE_SUCCESS'));
            }
          }
        });
      }
    });
  },

  onSendRoleUserEmail: function(owner, tool, evt) {
    let rec = owner.getRecord();
  },
  onRemoveRoleUser: function(owner, eOpts) {
    let rec = eOpts.record;
    if (rec.get('id') === Client.app.getController('Authentication').user.id) {
     Ext.Msg.alert(i18next.t('saa:ERROR'), i18next.t('saa:ERROR_REMOVE_ACCOUNT_USER_MINE'));
    } else {
      let template = new Ext.XTemplate(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_MESSAGE'));
      Ext.Msg.confirm(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_TITLE'), template.apply({rec: rec}), (btn) => {
        if (btn === 'yes') {
          rec.erase({
            callback: (record, operation, success) => {
              if (success !== true) {
                Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_ERROR'));
              } else {
                Ext.toast(i18next.t('saa:CONFIRM_REMOVE_ACCOUNT_USER_SUCCESS'));
              }
            }
          });
        }
      });
    }
  },

  onAddRole: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  let store = vm.getStore('roleStore');
	  let editor = Ext.create('Client.view.admin.role.Editor', {
	    handler: function(role) {
	      store.add(role);
	      store.sync({
          success: () => {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_SUCCESS'));
          },
          failure: () => {
            Ext.toast(i18next.t('saa:ADD_ACCOUNT_FAILURE'));
          }
        });
      }
    });
	  editor.show();
  },

  onRoleUserCancel: function(btn) {
    let me = this;
    let list = me.lookup('roleList');
    list.getSelectable().getSelectedRecords()[0].reject();
  },
  onRoleUserSave: function(btn) {
	  let me = this;
	  let list = me.lookup('roleList');
    let form = me.lookup('roleUserForm');
    if (form.validate()) {
      let record = list.getSelectable().getSelectedRecords()[0];
      btn.disable();
      record.save({
        success: () => {
          btn.enable();
        },
        failure: () => {
          Ext.toast(i18next.t('saa:ERROR_ACCOUNT_USER_SAVE_FAILURE'));
          btn.enable();
        }
      });
    }
  },
  onRoleUserGridSelect: function(grid, selected, eOpts) {
    let me = this;
    let panel = me.lookup('roleUserPanel');
    panel.setMasked(false);
  },
  onRoleUserGridDeselect: function(grid, selected, eOpts) {
	  let me = this;
    let panel = me.lookup('roleUserPanel');
    panel.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT_USER")
    });
    panel.reset();
  },

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	},

  onRefreshRoles: function() {
	  let me = this;
	  let view = me.getView();
	  let vm = view.getViewModel();
	  vm.getStore('roleStore').load();
  },

  onRoleListInitialize: function(list) {
	  let me = this;
    let view = me.getView();
    list.el.on({
      scope: me,
      contextmenu: me.onRoleContextMenu
    });
    Ext.Object.each(Client.tool.role, (tool) => {
      view.roleMenu.items.push(Client.tool.role[tool].config);
    });
    view.roleMenu.items.push({
      separator: true,
      handler: 'removeRoleHandler',
      bind: {
        text: '{"mod-admin-roles:REMOVE_ROLE":translate}',
        hidden: '{roleData.name==="Administrator" || roleData.name==="Developer" || roleData.name==="Account Owner" || roleData.name==="Account Manager"}'
      },
      margin: '10 0 0',
      iconCls: 'x-fa fa-remove'
    });
    if (view.roleMenu.items.length > 2) {
      view.roleMenu.items[1].separator = true;
    }
  },
  onRoleUserGridInitialize: function() {
	  let me = this;
	  let agGrid = me.lookup('roleUserGrid');
    agGrid.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT")
    });
  },
  onRoleUserFormInitialize: function() {
    let me = this;
    let panel = me.lookup('roleUserPanel');
    panel.setMasked({
      xtype: 'loadmask',
      message: i18next.t("saa:SELECT_ACCOUNT_USER")
    });
  },

  onRoleMenu: function (grid, context) {
    this.updateRoleMenu(context.record, context.tool.el, context.event, 'r-l?');
  },
  updateRoleMenu: function (record, el, e, align) {
    let menu = this.getRoleMenu();
    this.getViewModel().set('role', record);
    this.getViewModel().set('roleData', record.getData());
    menu.autoFocus = !e.pointerType;
    menu.showBy(el, align);
  },
  getRoleMenu: function () {
    let menu = this.roleToolMenu,
      view = this.getView();

    if (!menu) {
      this.roleToolMenu = menu = Ext.create(Ext.apply({
        ownerCmp: view
      }, view.roleMenu));
    }

    return menu;
  },
  onRoleContextMenu: function (e) {
    let list = this.lookup('roleList'),
      target = e.getTarget(list.itemSelector),
      item;

    if (target) {
      e.stopEvent();

      item = Ext.getCmp(target.id);
      if (item) {
        this.updateRoleMenu(item.getRecord(), item.el, e, 't-b?');
      }
    }
  }
});
