Ext.define('Client.view.admin.roles.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.admin.roles',
  requires: [
    'Ext.data.field.Date',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json'
  ],
	data: {
		name: 'Client',
    role: {},
    extraRoleMapParams: {
		  filter: '{"where": {}, "include": [{"relation": "user", "scope": {"fields": ["username","email"]} }] }'
    }
	},
  stores: {
	  roleMapStore: {
	    autoLoad: false,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        type: 'date',
        name: 'created'
      }, {
        type: 'date',
        name: 'modified'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/RoleMappings',
        extraParams: '{extraRoleMapParams}',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PATCH",
          destroy:"DELETE"
        },
        reader: {
          type: 'json'
        }
      }
    },
	  userStore: {
	    autoLoad: true,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'realm'
      }, {
        name: 'username'
      }, {
        name: 'email'
      }, {
        name: 'emailVerified'
      }, {
        name: 'password'
      }, {
        name: 'roles'
      }, {
        name: 'accountId'
      }, {
        name: 'account'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/Roles/{role.id}/users',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PUT",
          destroy:"DELETE"
        },
        reader: {
          type: 'json',
          rootProperty: 'data'
        }
      }
    },
	  roleStore: {
	    autoLoad: true,
      fields: [{
        name: 'id',
        persist: false
      }, {
        name: 'name'
      }, {
        name: 'description'
      }, {
        type: 'date',
        name: 'created'
      }, {
        type: 'date',
        name: 'modified'
      }],
      proxy: {
        type: 'rest',
        url: '{apiUrl}/Roles',
        actionMethods: {
          create:"POST",
          read:"GET",
          update:"PATCH",
          destroy:"DELETE"
        },
        reader: {
          type: 'json',
          rootProperty: 'data'
        }
      }
    }
  }
});
