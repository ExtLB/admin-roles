Ext.define('Client.view.admin.role.EditorController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.admin.role.editor',

  okHandler: function(btn) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (view.handler) {
      view.handler(vm.get('role'));
    }
    view.destroy();
  },
  cancelHandler: function(btn) {
    this.getView().destroy();
  },

  onInitialize: function (editor) {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    if (!_.isUndefined(view.role)) {
      vm.set('role', view.role);
      vm.set('action', 'edit');
      view.setBind({
        title: '{"saa:EDIT_ROLE":translate} {role.name}',
      });
    } else {
      vm.set('role', {
        name: '',
        description: '',
      });
    }
  }
});
