Ext.define('Client.view.admin.role.Editor', {
  extend: 'Ext.Dialog',
  xtype: 'admin.role.editor',
  requires: [
    'Ext.Toolbar',
    'Ext.Button',
    'Ext.Spacer',
    'Ext.form.Panel',
    'Ext.field.Select',
    'Ext.field.TextArea',
    'Ext.dataview.List'
  ],
  viewModel: {type: 'admin.role.editor'},
  controller: {type: 'admin.role.editor'},
  bind: {
    title: '{"mod-admin-roles:ADD_ROLE":translate} {role.name}',
  },
  layout: 'fit',
  closable: true,
  height: 400,
  width: 300,
  listeners: {
    initialize: 'onInitialize'
  },
  items: [{
    xtype: 'formpanel',
    reference: 'roleForm',
    layout: 'vbox',
    padding: 10,
    items: [{
      xtype: 'textfield',
      name: 'name',
      required: true,
      bind: {
        label: '{"mod-admin-roles:ROLE_NAME":translate}',
        value: '{role.name}'
      }
    }, {
      xtype: 'textareafield',
      name: 'description',
      required: true,
      flex: 1,
      bind: {
        label: '{"mod-admin-roles:ROLE_DESCRIPTION":translate}',
        value: '{role.description}'
      }
    }]
  }, {
    xtype: 'toolbar',
    docked: 'bottom',
    items: [{
      xtype: 'button',
      text: 'Ok',
      handler: 'okHandler'
    }, {
      xtype: 'spacer'
    }, {
      xtype: 'button',
      text: 'Cancel',
      handler: 'cancelHandler'
    }]
  }]
});
