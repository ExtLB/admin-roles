const async = require('async');
let log = require('@dosarrest/loopback-component-logger')('mod-admin-roles/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let app = me.app;
    let done = me.done;
    let registry = app.registry;
    let ACL = registry.getModelByType('ACL');
    let Role = registry.getModelByType('Role');
    let RoleMapping = registry.getModelByType('RoleMapping');
    let AdminNavigation = registry.getModelByType('AdminNavigation');
    async.series({
      AdminNavigation: (cb) => {
        AdminNavigation.find({where: {routeId: 'roles', perspective: 'admin'}}).then(navItem => {
          if (navItem.length === 0) {
            AdminNavigation.create({
              text: 'mod-admin-roles:ROLES',
              perspective: 'admin',
              iconCls: 'x-fa fa-users',
              // rowCls: 'nav-tree-badge nav-tree-badge-new',
              viewType: 'roles',
              routeId: 'roles',
              leaf: true,
            }).then(newNavItem => {
              log.info(newNavItem);
              cb(null, true);
            }).catch(err => {
              log.error(err);
              cb(null, false);
            });
          } else {
            cb(null, true);
          }
        }).catch(err => {
          log.error(err);
          cb(null, true);
        });
      },
      RoleEveryoneACL: (cb) => {
        ACL.findOrCreate({
          model: Role.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: '$everyone',
          permission: 'DENY',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      },
      RoleAdminACL: (cb) => {
        ACL.findOrCreate({
          model: Role.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: 'Administrator',
          permission: 'ALLOW',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      },
      RoleMappingEveryoneACL: (cb) => {
        ACL.findOrCreate({
          model: RoleMapping.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: '$everyone',
          permission: 'DENY',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      },
      RoleMappingAdminACL: (cb) => {
        ACL.findOrCreate({
          model: RoleMapping.definition.name,
          accessType: '*',
          principalType: 'ROLE',
          principalId: 'Administrator',
          permission: 'ALLOW',
          property: '*'
        }).then((acl) => {
          cb(null, true);
        }).catch((err) => {
          log.error(err);
          cb(null, true)
        });
      }
    }, (err, results) => {
      done(null, true);
    });
  }
}
module.exports = Boot;
